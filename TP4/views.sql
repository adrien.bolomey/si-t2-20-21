/* Chapter 6 (Groupe des questions 3) */
/* 1. */
CREATE OR REPLACE VIEW TweetRecent
AS (
    SELECT TweetText, DateCreation
    FROM Tweets
    /*WHERE datecreation >= SYSDATE-3*/
    WHERE datecreation >= SYSDATE-14 /* Required to get some entries since the dates are not recent */
);

COMMIT; /* Create view and "backup" the data of the db */

/* Insert test which should not work because the view does not have access to all columns which can't be NULL in order to be able to insert*/
INSERT INTO TweetRecent VALUES ('Test', SYSDATE-1);

/* Update test which should only work when the view returns some entries */
UPDATE TweetRecent
SET TweetText='Test update trough TweetRecent view', datecreation=SYSDATE-1;

/* Delete test which should only work when the view returns some entries */
DELETE FROM TweetRecent;

ROLLBACK;

/*
    Insert not possible because the view does not have "access" to the pk and the pk can't be NULL
    Update works as long the view returns some entries, meaning it updates only the entries which are returned from the view
    Same like update for delete. It only deletes the entries which are returned by the view
*/

/* Test queries */
SELECT *
FROM TweetRecent;

SELECT *
FROM Tweets;

/* 2. */
CREATE OR REPLACE VIEW userValide
AS (
    SELECT *
    FROM userst
    WHERE statut ='valide'
) WITH CHECK OPTION;

COMMIT;

/* Valid insert, since default of statut is valide */
INSERT INTO userValide (userid, name, email)
VALUES (4, 'valideInsertTest', 'insertViaView@gmail.com');

/* Invalid insert, since statut is set to invalide */
INSERT INTO userValide (userid, name, email, statut)
VALUES (4, 'valideInsertTest', 'insertViaView@gmail.com', 'invalide');

/* Update: Set the statut to invalide of all the valid entries */
UPDATE userValide
SET statut='invalide';
/* Update: Set the urlprofilimage to NULL of all the valid entries */
UPDATE userValide
SET urlprofilimage=NULL;

/* Delete every entry */
DELETE FROM userValide;

/* Delete the invalid entries via userValide (Should delete 0 entries) */
DELETE FROM userValide
WHERE statut='invalide';

ROLLBACK;

/*
    Insert:
        Valid insert possible by using the default value for statut (which is valide)
        Valid insert possible by manually setting the statut to valide
        Invalid insert possible by manually setting the status to invalide or bani
    Update:
        Setting all valid entries to invalid, does not work because of the WITH CHECK OPTION
        Setting the urlprofilimage of all valid entries to NULL, works since the WITCH CHECK OPTION works for statut and not the rest
    Delete:
        Deleting all entries which are returned by the view works
        Deleting all invalid entries via the userValide view does not work since the view only returns valid entries
*/

/* Test queries */
SELECT *
FROM userValide;

SELECT *
FROM userst;

/* 3. */
CREATE OR REPLACE VIEW NBtweetPerUser
AS (
    SELECT userId, COUNT(*) NbTweet
    FROM tweets
    GROUP BY userId
);

COMMIT;

/* Insert test which should not work */
INSERT INTO NBtweetPerUser
VALUES (4, 7);

/* Update test which should not work */
UPDATE NBtweetPerUser
SET userid=4;

/* Delete test which should not work */
DELETE FROM NBtweetPerUser;

ROLLBACK;

/*
    None of them should work because the view uses a count and a group by for creating it
    Insert: Error= Virtual column not allowed here
    Update: Error= Data manipulation operation not legal on this view
    Delete: Error= Data manipulation operation not legal on this view
*/

/* Test queries */
SELECT *
FROM NBtweetPerUser;

SELECT *
FROM tweets;