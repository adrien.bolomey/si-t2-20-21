--2
CREATE OR REPLACE VIEW userValide
AS (
    SELECT *
    FROM users
    WHERE statut ='valide'
) WITH CHECK OPTION;
