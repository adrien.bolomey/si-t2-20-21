-- Les modifcations necessaires pour repondre a la demande 5
ALTER TABLE tweets
    drop constraint fk_tweets;
    
ALTER TABLE quisuitqui
    drop constraint fk_follow;
    
ALTER TABLE quisuitqui
    drop constraint fk_follower;
    
ALTER TABLE tweettag
    drop constraint fk_tweetag;

ALTER TABLE tweets
    add constraint fk_tweets_2 foreign key (USERID) 
    references Users(USERID)
    on delete cascade;
    
ALTER TABLE quisuitqui
    add constraint fk_follow_2 foreign key (UserIDQuiSuit) 
    references Users(USERID)
    on delete cascade;

ALTER TABLE quisuitqui
    add constraint fk_follower_2 foreign key (UserIDDuSuivi) 
    references Users(USERID)
    on delete cascade;
    
ALTER TABLE tweettag
    add constraint fk_tweettag_2 foreign key (TweetID) 
    references Tweets(TweetID)
    on delete cascade;