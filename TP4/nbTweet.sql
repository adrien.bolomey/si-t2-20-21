CREATE OR REPLACE VIEW NBtweetPerUser
AS (
    SELECT userId, COUNT(*) NbTweet
    FROM tweets
    GROUP BY userId
);
