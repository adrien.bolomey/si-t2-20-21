create table Users (
    UserID int primary key,
    name varchar2(20) null,
    UrlProfilImage varchar2(255) null,
    email varchar2(20) not null unique, --6
    statut varchar2(20) default 'valide' not null,
    DateCreation date default sysdate not null, --5
    DateLastUpdate date null,
    constraint statutCheck check (statut='valide' or statut='invalide'), --2
    constraint email check (email like '%@gmail.com'), --4
    constraint DateLastUpdate check (DateLastUpdate >= DateCreation) --7
);