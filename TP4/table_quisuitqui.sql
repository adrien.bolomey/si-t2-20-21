CREATE TABLE QuiSuitQui(
    UserIdQuiSuit int,
    UserIdDuSuivi int,
    DateDebutSuivi date not null,
    CONSTRAINT PK PRIMARY KEY (UserIdQuiSuit, UserIdDuSuivi),
    constraint fk_follow FOREIGN KEY (UserIdQuiSuit) REFERENCES USERS(USERID),
    constraint fk_follower FOREIGN KEY (UserIdDuSuivi) REFERENCES USERS(USERID)
);