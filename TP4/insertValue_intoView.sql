/* Valid insert, since default of statut is valide */
INSERT INTO userValide (userid, name, email)
VALUES (4, 'valideInsertTest', 'insert@gmail.com');

/* Invalid insert, since statut is set to invalide */
INSERT INTO userValide (userid, name, email, statut)
VALUES (5, 'valideInsertTest', 'insert@gmail.com', 'invalide');