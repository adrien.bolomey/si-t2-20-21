CREATE TABLE TweetTag(
    idtt int primary key,
    TweetID int null, --1
    tag varchar(50) not null,
    constraint fk_tweetag FOREIGN KEY (TweetID) REFERENCES TWEETS(TweetID),
    constraint unicityTweet UNIQUE(TweetID, tag) --8
);