CREATE TABLE Tweets (
    TweetID INT PRIMARY KEY,
    TweetText VARCHAR2(255) not null,
    DateCreation DATE default SYSDATE not null, --1
    UrlProfilImage VARCHAR2(255) null, --1
    UserID INT not null,
    constraint fk_tweets FOREIGN KEY (UserID) REFERENCES USERS(USERID)
);