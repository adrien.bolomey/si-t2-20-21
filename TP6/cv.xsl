<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="2.0">
	
	<xsl:output method="html"/>
	<xsl:strip-space elements="*" />
	
	<xsl:template match="/">
		<xsl:apply-templates select="CV" mode="html"/>
		<xsl:apply-templates select="CV" mode="txt"/>
	</xsl:template>
	
	<xsl:template match="CV" mode="html">
		<html>
			<body>
				<h1> Mon CV du <xsl:value-of select="./@CREATION_DATE"/></h1>
				<img alt="Photo" width="200" height="200">
					<xsl:attribute name="SRC">
						<xsl:value-of select="./DONNEES_PERSO/PHOTO/@SRC"/>
					</xsl:attribute>
				</img>
				<ul>
					<li><xsl:value-of select="./DONNEES_PERSO/PRENOM"/><xsl:text> </xsl:text><xsl:value-of select="./DONNEES_PERSO/NOM"/></li>
					<li>Je connais <xsl:value-of select="count(LANGUES/LANGUE)"/> langues à savoir:</li>
					<li>J'ai à mon actif <xsl:value-of select="ETUDES"/> années d'études.</li>
					<li>J'ai travaillé sur <xsl:value-of select="count(EXPERIENCES/PROJETS)"/> projet(s) et occupés <xsl:value-of select="count(EXPERIENCES/POSTES)"/> poste(s)</li>
				</ul>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="CV" mode="txt">
		<xsl:result-document href="paramConnexion.txt">	
			Les données de connexion de <xsl:value-of select="./DONNEES_PERSO/PRENOM"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="./DONNEES_PERSO/NOM"/>
			<xsl:apply-templates select="./CONNEXIONS_DB/DONNEESCONNEXION"/>
		</xsl:result-document>
	</xsl:template>
	
	<xsl:template match="DONNEESCONNEXION">
		/*SGBD <xsl:value-of select="./@BD"/>*/
	Base donnée: <xsl:value-of select="./@NAME"/>
	Login: <xsl:value-of select="LOGIN"/>
	Password: <xsl:value-of select="PASSWORD"/>
	</xsl:template>
</xsl:stylesheet>