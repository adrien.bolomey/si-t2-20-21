select distinct n_dep from livraison
where n_dep not in 
(select l.n_dep
    from depot d, livraison l,journal j
    where d.n_dep = l.n_dep
    and l.code_j = j.code_j
    and adr = adr_j)
order by n_dep;