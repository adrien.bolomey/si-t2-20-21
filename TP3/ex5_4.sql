select distinct journal.code_j 
from journal
inner join livraison on livraison.code_j=journal.code_j
inner join depot on livraison.n_dep=depot.n_dep
where depot.adr LIKE 'PARIS'
order by journal.code_j;