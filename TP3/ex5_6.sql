--a--
select n_dep
from depot
where n_dep not in(select n_dep from livraison)
order by n_dep;
--b--
select n_dep
from depot
where not exists(select n_dep from livraison
                 where livraison.n_dep = depot.n_dep  )
order by n_dep;
--c--
select n_dep 
from depot
minus
select n_dep 
from livraison;
--d--
select depot.n_dep
from depot
left join livraison on depot.n_dep=livraison.n_dep
where livraison.n_dep is null
order by depot.n_dep;
