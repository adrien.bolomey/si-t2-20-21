select l.n_dep
from livraison l
    inner join journal j on (l.code_j = j.code_j)
where j.type = 'FEMININ'
group by l.n_dep
having count(distinct l.code_j) = (
    select count(j.type)
    from journal j
    where j.type = 'FEMININ');