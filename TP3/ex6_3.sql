select distinct depot.n_dep
from depot
inner join livraison on livraison.n_dep = depot.n_dep
inner join journal on journal.code_j=livraison.code_j
where journal.adr_j != depot.adr
order by depot.n_dep;