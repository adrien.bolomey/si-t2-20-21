/**
 * Title:
 * Description: Afficher via ce programme le contenu d'une table de son compte oracle
 * Copyright:    Copyright (c) 2020
 * Company:
 * @author       Houda Chabbi
 * @version 1.0
 * compilation: javac lire_journaux_oracle12c_t2_0.java -classpath .
 * execution: java -cp .:ojdbc8.jar lire_journaux_oracle12c_t2_0
 * si pb de droit chmod 744 ojdbc8.jar

 * sur oracle on a la table JOURNAUX du TP LMD

Exemple de données:

CODE_J TITRE                 PRIX TYPE       PERIODE    ADR_J
---------- --------------- ---------- ---------- ---------- ----------
115 LIBERATION             4.5 INFO       QUOTIDIEN  PARIS
110 LE MONDE               4.5 INFO       QUOTIDIEN  PARIS
120 LA RECHERCHE            32 SCIENCE    MENSUEL    PARIS


 */

// You need to import the java.sql package to use JDBC
import java.sql.*;

// We import java.io to be able to read from the command line
import java.io.*;
import java.util.*;

public class lire_journaux_mysql_t2_0 {
    public static void main (String args [])
            throws SQLException {
        System.out.print ("C'est parti...");
        System.out.flush ();

        //step1: create a connection after loading the driver class
        Connection conn=connexion();
        //step2: use the connection to execute a select
        interrogation(conn);
        //step3: close the connection
        deconnexion(conn);
    }

    static Connection connexion()
            throws SQLException {

        System.out.print ("connexion...");
        System.out.flush ();

        try
        {// chargement du driver
            Class.forName("com.mysql.jdbc.Driver"); //Modifier driver pour mysql
        }
        catch( Exception e ) { //pb. charg. du driver
            // class inexistante?
            System.out.println ("Oops ... problème avec le driver inexistant ");
            e.printStackTrace();
            System.exit(1);
        }

        // Connect to the database
        System.out.print ("Connecting to the database...");
        System.out.flush ();

        String db_name   = "tpjdbc03";
        String url       = "jdbc:mysql://locahost/" + db_name; //Regarder slide pour mysql localhost et port 3306
        String user_id   = "root";
        String user_pass = "";

        Connection conn=null;
        try
        {
            conn=DriverManager.getConnection (url, user_id, user_pass);
            System.out.println ("connected.");
        }
        catch (SQLException e)
        {
            System.err.println("connection failed");
            System.err.println(e);
            System.exit(1);
        }

        return conn;

    }

    static void interrogation(Connection conn)
            throws SQLException {
        System.out.println ("--------------- Affichage ---------------");
        System.out.println ("Voici le contenu de votre table actuellement");
        System.out.println ("-->Seules les colonnes Titre et Prix seront affichées");

        // Create a Statement
        Statement stmt = conn.createStatement ();
        // Select the TITRE and PRIX columns from the JOURNAL table
        //ResultSet rset = stmt.executeQuery ("select TITRE, PRIX, TYPE from JOURNAL");

        // Select the TITRE and PRIX columns from the JOURNAL table where type = info and adr_j = paris
        // ResultSet rset = stmt.executeQuery ("select TITRE, PRIX, PERIODE from journal_t2_6 where TYPE = 'INFO' and ADR_J = 'PARIS'");
        ResultSet rset = stmt.executeQuery ("select TITRE, PRIX, PERIODE from journal");

        // Iterate through the result and print the employee names
        while (rset.next ())
            System.out.println ("-> Titre: " + rset.getString(1) +
                    "\t  Prix: " + rset.getFloat("PRIX") +
                    "\t  Periode: " + rset.getString("PERIODE"));
        // Close the ResultSet
        rset.close();

        // Close the Statement
        stmt.close();
    }

    static void deconnexion(Connection conn)
            throws SQLException {
        conn.close();
    }
}