import java.sql.Connection;
import java.sql.SQLException;

public class SQL {
    public static void main(String[] args) throws SQLException {
        //step1: create a connection after loading the driver class
        Connection connMysql=lire_journaux_mysql_t2_0.connexion();
        Connection connOracle=lire_journaux_oracle12c_t2_0.connexion();
        //step2: use the connection to execute a select
        lire_journaux_mysql_t2_0.interrogation(connMysql);
        lire_journaux_oracle12c_t2_0.interrogation(connOracle);
        //step3: close the connection
        lire_journaux_mysql_t2_0.deconnexion(connMysql);
        lire_journaux_oracle12c_t2_0.deconnexion(connOracle);
    }
}

