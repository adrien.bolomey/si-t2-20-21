/**
 * Title:
 * Description: Afficher via ce programme le contenu d'une table de son compte oracle
 * Copyright:    Copyright (c) 2020
 * Company:
 * @author       Houda Chabbi
 * @version 1.0
 * compilation: javac lire_journaux_oracle12c_t2_0.java -classpath .
 * execution: java -cp .:ojdbc8.jar lire_journaux_oracle12c_t2_0 
 * si pb de droit chmod 744 ojdbc8.jar

 * sur oracle on a la table JOURNAUX du TP LMD

Exemple de données: 

CODE_J TITRE                 PRIX TYPE       PERIODE    ADR_J
---------- --------------- ---------- ---------- ---------- ----------
115 LIBERATION             4.5 INFO       QUOTIDIEN  PARIS
110 LE MONDE               4.5 INFO       QUOTIDIEN  PARIS
120 LA RECHERCHE            32 SCIENCE    MENSUEL    PARIS
 */

// You need to import the java.sql package to use JDBC
import java.sql.*;

// We import java.io to be able to read from the command line
import java.io.*;
import java.util.*;

public class lire_journaux_oracle12c_t2_0 {
    public static void main (String args [])
            throws SQLException {
        System.out.print ("C'est parti...");
        System.out.flush ();

        //step1: create a connection after loading the driver class
        Connection conn=connexion();
        //step2: use the connection to execute a select
        interrogation(conn);
        //step3: close the connection
        deconnexion(conn);
    }

    static Connection connexion()
            throws SQLException {

        System.out.print ("connexion...");
        System.out.flush ();

        try
        {// chargement du driver
            Class.forName("oracle.jdbc.driver.OracleDriver");
        }
        catch( Exception e ) { //pb. charg. du driver
            // class inexistante?
            System.out.println ("Oops ... problème avec le driver inexistant ");
            e.printStackTrace();
            System.exit(1);
        }

        // Connect to the database
        System.out.print ("Connecting to the database...");
        System.out.flush ();

        String db_name   = "DBINF";
        String url       = "jdbc:oracle:thin:@160.98.2.106:9203:" + db_name;
        String user_id   = "t2-3";
        String user_pass = "T2-3";

        Connection conn=null;
        try
        {
            conn=DriverManager.getConnection (url, user_id, user_pass);
            System.out.println ("connected.");
        }
        catch (SQLException e)
        {
            System.err.println("connection failed");
            System.err.println(e);
            System.exit(1);
        }

        return conn;

    }

    static void interrogation(Connection conn)
            throws SQLException {
        System.out.println ("--------------- Affichage ---------------");
        System.out.println ("Voici le contenu de votre table actuellement");
        System.out.println ("-->Seules les colonnes Titre et Prix seront affichées");

        // Create a Statement
        Statement stmt = conn.createStatement ();
        // Select the TITRE and PRIX columns from the JOURNAL table
        ResultSet rset = stmt.executeQuery ("select TITRE, PRIX, 'PERIOD' as PERIOD from JOURNAL where type='INFO' and ADR_J='PARIS'");
        // Iterate through the result and print the employee names
        while (rset.next ())
            System.out.println ("-> Titre: " + rset.getString(1) +
                    "\t Prix: " + rset.getFloat("PRIX") + "\t Period: " + rset.getString(3));
        // Close the ResultSet
        rset.close();

        // Close the Statement
        stmt.close();
    }

    static void deconnexion(Connection conn)
            throws SQLException {
        conn.close();
    }
}